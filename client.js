#!/usr/bin/env node
const ws = require('ws');
const WebSocket = require('reconnecting-websocket');
const fs = require('fs');
const { format } = require('date-fns');
const uuid = require('uuid');
const chalk = require('chalk');
const os = require('os');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const Netmask = require('netmask').Netmask;
const ping = require('ping');
const arp = require('node-arp');

const configFileData = fs.readFileSync(process.argv[2]);
const config = JSON.parse(configFileData);

const cabinets = [];
const servers = [];
const files = [];

const IGNORED_TYPES = ['updateTeamData', 'showPreMatchScreen'];

const wsOptions = {
  WebSocket: ws,
  connectionTimeout: 10000,
};

function log(...message) {
  console.log(chalk.gray(format(new Date(), 'yyyy-MM-dd HH:mm:ss')), ...message);
}

function logCabinetName(cabinet) {
  if (cabinet.sceneName && cabinet.cabinetName) {
    return chalk.yellow(`${cabinet.sceneName}/${cabinet.cabinetName}`);
  }

  if (cabinet.deviceId) {
    return chalk.yellow(cabinet.deviceId);
  }
}

function initialize() {
  for (const cabinetConfig of config.cabinets) {
    const cabinet = {
      config: cabinetConfig,
      socket: null,
      initConnection: false,
      servers: [],
      files: [],
    };

    resetGame(cabinet);
    cabinets.push(cabinet);
  }

  if (config.servers) {
    for (const serverConfig of config.servers) {
      servers.push({
        config: serverConfig,
        socket: null,
        initConnection: false,
      });
    }
  }

  if (config.files) {
    for (const fileConfig of config.files) {
      files.push({
        config: fileConfig,
        handle: null,
      });
    }
  }
}

function connect() {
  for (const cabinet of cabinets) {
    connectCabinet(cabinet);
  }

  for (const server of servers) {
    server.messages = [];
    server.acknowledged = new Set();
    connectServer(server);
  }

  for (const file of files) {
    file.handle = fs.createWriteStream(file.config.filename, { flags: 'a' });
  }
}

async function connectCabinet(cabinet) {
  cabinet.isAlive = true;
  cabinet.messages = [];
  cabinet.gameInProgress = false;
  cabinet.lastGameComplete = 0;

  if (cabinet.socket) {
    cabinet.socket.close(); //ensure we don't open a duplicate connection
    cabinet.socket = null;
  }

  if (cabinet.config.url) {
    cabinet.url = cabinet.config.url;
  } else if (cabinet.config.mac) {
    const ip = await findAddressByMAC(cabinet.config.mac, cabinet.config.subnet);
    if (ip === undefined) {
      cabinet.socket = null;
      cabinet.reconnect = setTimeout(() => connectCabinet(cabinet), 10000);
      log(`Could not connect to cabinet ${logCabinetName(cabinet.config)}: cannot find MAC on local network`);
      return;
    }

    cabinet.url = `ws://${ip}:12749`;
  } else {
    log(`Invalid config for cabinet ${logCabinetName(cabinet.config)}: 'url' or 'mac' is required`);
    return;
  }

  cabinet.socket = new WebSocket(cabinet.url, [], wsOptions);
  cabinet.socket.addEventListener('open', () => {
    if (cabinet.socket && cabinet.socket.readyState === WebSocket.OPEN) {
      log(`Connected to cabinet ${logCabinetName(cabinet.config)} at ${cabinet.url}`);
      cabinet.initConnection = true;
      sendMessage({ type: 'cabinetOnline', values: [] }, cabinet);
    }
  });

  cabinet.socket.addEventListener('message', message => {
    cabinet.isAlive = true;
    const parsedMessage = parseMessage(message);
    if (parsedMessage === null)
      return;

    if (parsedMessage.type == 'alive') {
      if (cabinet.socket.readyState === WebSocket.OPEN) {
        const aliveMessage = '![k[im alive],v[null]]!';
        cabinet.socket.send(aliveMessage);
      }

      return;
    }

    if (parsedMessage.type === 'enteredGameScreen') {
      if (cabinet.showPreMatchScreen) {
        cabinet.messages.push({ type: 'showPreMatchScreen' });
        cabinet.showPreMatchScreen = false;
      }

      return;
    }

    if (IGNORED_TYPES.includes(parsedMessage.type)) {
      return;
    }

    sendMessage(parsedMessage, cabinet);

    if (parsedMessage.type === 'mapstart' || parsedMessage === 'gamestart') {
      cabinet.gameInProgress = true;
    }

    if (parsedMessage.type === 'victory') {
      log(`Game complete (${parsedMessage.values[0]} ${parsedMessage.values[1]}) on ${logCabinetName(cabinet.config)}`);
      resetGame(cabinet);
      cabinet.lastGameComplete = Date.now();
    }
  });

  cabinet.socket.addEventListener('close', ({ code, reason }) => {
    log(`Cabinet ${logCabinetName(cabinet.config)} disconnected: (${code}) ${reason}`);
    sendMessage({ type: 'cabinetOffline', values: [code, reason] }, cabinet);
  });

  cabinet.socket.addEventListener('error', error => {
    log(`Error connecting to cabinet ${logCabinetName(cabinet.config)}: ${error.message}`);
    if (!cabinet.initConnection) {
      log(`Initial connection to ${logCabinetName(cabinet.config)} failed, retrying in 10 seconds`);
      if (cabinet.reconnect)
        clearTimeout(cabinet.reconnect);

      cabinet.reconnect = setTimeout(() => connectCabinet(cabinet), 10000);
    }
  });

  cabinet.sendMessages = () => {
    const messageBatch = cabinet.messages.splice(0, Math.min(cabinet.messages.length, 1));
    for (const message of messageBatch) {
      const values = message.values ? message.values.join(',') : 'null';
      const messageText = `![k[${message.type}],v[${values}]]!`;
      cabinet.socket.send(messageText + '\n');
    }
  };

  cabinet.sendMessagesInterval = setInterval(cabinet.sendMessages, 100);

  if (cabinet.pingInterval)
    clearInterval(cabinet.pingInterval);

  cabinet.pingInterval = setInterval(() => {
    if (!cabinet.isAlive) {
      cabinet.socket.close();
      return;
    }

    cabinet.isAlive = false;
    if (cabinet.socket.readyState === WebSocket.OPEN) {
      cabinet.socket._ws.ping();
    }
  }, 30000);
}

function connectServer(server) {
  if (server.socket) {
    server.socket.close();
    server.socket = null;
  }

  server.socket = new WebSocket(server.config.url, [], wsOptions);

  server.socket.addEventListener('open', () => {
    if (server.socket && server.socket.readyState === WebSocket.OPEN) {
      log(`Connected to server ${chalk.blue(server.config.name)}`);
      server.initConnection = true;
    }

    for (const cabinet of cabinets) {
      if (cabinet.isAlive) {
        sendMessage({ type: 'cabinetOnline', values: [] }, cabinet);
      }
    }
  });

  server.socket.addEventListener('message', message => {
    const messageData = JSON.parse(message.data);

    if (messageData.error) {
      log(`${chalk.red('Error from server')} ${chalk.blue(server.config.name)}: ${messageData.error}`);
    }

    if (messageData && messageData.type === 'match') {
      if (messageData.match_type === 'tournament') {
        const updateData = {};
        let disconnect = false;

        if (messageData && messageData.current_match) {
          updateData.bracketURL = messageData.current_match.bracket_url;
          updateData.warmup = messageData.current_match.is_warmup.toString();
          updateData.winsNeeded = messageData.current_match.wins_per_match ||
            messageData.current_match.rounds_per_match ||
            0;

          updateData.blueTeam = {
            name: messageData.current_match.blue_team.substring(0, 25) || '',
            score: messageData.current_match.blue_score || 0,
            wins: messageData.current_match.blue_score || 0,
            losses: messageData.current_match.gold_score || 0,
          };

          updateData.goldTeam = {
            name: messageData.current_match.gold_team.substring(0, 25) || '',
            score: messageData.current_match.gold_score || 0,
            wins: messageData.current_match.gold_score || 0,
            losses: messageData.current_match.blue_score || 0,
          };
        } else {
          updateData.bracketURL = '';
          updateData.warmup = false;
          updateData.winsNeeded = 0;

          updateData.blueTeam = updateData.goldTeam = {
            name: '',
            score: 0,
            wins: 0,
            losses: 0,
          };

          disconnect = true;
        }

        for (const cabinet of cabinets) {
          if (cabinet.config.sceneName === messageData.scene_name &&
              cabinet.config.cabinetName === messageData.cabinet_name) {

            cabinet.messages.push({ type: 'updateTeamData', values: [JSON.stringify(updateData)] });

            if (messageData.current_match) {
              if (messageData.current_match.blue_score === 0 &&
                  messageData.current_match.gold_score === 0) {

                if (!cabinet.gameInProgress) {
                  if (cabinet.lastGameComplete && cabinet.lastGameComplete > Date.now() - 15000) {
                    cabinet.showPreMatchScreen = true;
                  } else {
                    cabinet.messages.push({ type: 'showPreMatchScreen' });
                  }
                }
              }
            } else {
              cabinet.messages.push({ type: 'disconnectHivemind' });
            }
          }
        }
      }

      return;
    }

    if (messageData.type === 'signin') {
      const updateData = {
        playerName: messageData.user_name,
        playerNumber: messageData.player_id.toString(),
      };

      const action = messageData.action === 'sign_in' ? 'signInPlayer' : 'signOutPlayer';
      for (const cabinet of cabinets) {
        if (cabinet.config.sceneName === messageData.scene_name &&
              cabinet.config.cabinetName === messageData.cabinet_name) {

          cabinet.messages.push({ type: action, values: [JSON.stringify(updateData)] });
        }
      }

      return;
    }

    if (messageData.eventID) {
      server.acknowledged.add(messageData.eventID);
    }
  });

  server.socket.addEventListener('error', error => {
    log(`Error connecting to server ${chalk.blue(server.config.name)}: ${error.message}`);
    if (!server.initConnection) {
      log(`Initial connection to ${chalk.blue(server.config.name)} failed, retrying in 10 seconds`);
      if (server.reconnect)
        clearTimeout(server.reconnect);

      server.reconnect = setTimeout(() => connectServer(server), 10000);
    }
  });

  server.socket.addEventListener('close', ({ code, reason }) => {
    log(`Server ${chalk.blue(server.config.name)} disconnected: ${chalk.red(code)} ${chalk.red(reason)}`);
  });

  server.messageInterval = setInterval(() => {
    if (server.socket && server.socket.readyState == WebSocket.OPEN) {
      const resendBefore = Date.now() - 1000;

      const messageBatch = server.messages.splice(0, server.messages.length);
      for (const message of messageBatch) {
        if (server.acknowledged.has(message.eventID))
          continue;

        if (!message.lastAttempt || message.lastAttempt < resendBefore) {
          message.lastAttempt = Date.now();
          server.socket.send(message.text);
        }

        server.messages.push(message);
      }

      if (server.messages.length == 0) {
        server.acknowledged.clear();
      }
    }
  }, 100);

  server.logInterval = setInterval(() => {
    const connectedText = server.socket && server.socket.readyState == WebSocket.OPEN ?
          chalk.green('connected') :
          chalk.red('not connected');

    log(`Server ${chalk.blue(server.config.name)} is ${connectedText}. ${chalk.bold(server.messages.length)} outstanding messages.`);
  }, 15000);
}

function parseMessage(message) {
  const parsedMessage = String(message.data).match(/!\[k\[(\w+)[\:\s]*\],v\[(.*)?\]\]!/);
  if (!parsedMessage) {
    return null;
  }

  return {
    type: parsedMessage[1],
    values: parsedMessage[2] && parsedMessage[2].split(',') || [],
  };
}

function resetGame(cabinet) {
  cabinet.currentGame = uuid.v4();
  cabinet.gameInProgress = false;
}

function createMessage(message, cabinet) {
  return {
    gameID: cabinet.currentGame,
    eventID: uuid.v4(),
    time: new Date(),
    type: message.type,
    values: message.values,
    cabinet: {
      sceneName: cabinet.config.sceneName,
      cabinetName: cabinet.config.cabinetName,
      deviceId: cabinet.config.deviceId,
      token: cabinet.config.token,
    },
  };
}

function sendMessage(message, cabinet) {
  const outgoing = createMessage(message, cabinet);
  const messageText = JSON.stringify(outgoing);

  for (const server of servers) {
    server.messages.push({ eventID: outgoing.eventID, text: messageText });
  }

  for (const file of files) {
    if (file.handle) {
      file.handle.write(messageText + '\n');
    }
  }
}

function getMAC(ip) {
  return new Promise((resolve, reject) => {
    ping.promise.probe(ip).then(response => {
      if (response.alive) {
        arp.getMAC(ip, (err, mac) => {
          if (err) {
            reject(err);
          } else {
            resolve(mac);
          }
        });
      } else {
        reject('down');
      }
    });
  });
}

function findAddressByMAC(mac, subnet) {
  return new Promise((resolve, reject) => {
    const netmask = new Netmask(subnet);
    const promises = [];
    netmask.forEach(addr => {
      promises.push(
        getMAC(addr).then(addrMAC => {
          if (mac == addrMAC) {
            resolve(addr);
          }
        }).catch(err => {})
      );
    });

    Promise.all(promises).then(() => {
      resolve(undefined);
    });
  });
}

initialize();
connect();

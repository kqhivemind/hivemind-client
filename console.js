#!/usr/bin/env node
const ws = require("ws");
const WebSocket = require("reconnecting-websocket");
const readline = require("node:readline");
const chalk = require("chalk");
const { format } = require("date-fns");
const util = require("util");

const rl = readline.promises.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const wsOptions = {
  WebSocket: ws,
  connectionTimeout: 10000,
};

const parseMessage = (message) => {
  const parsedMessage = String(message.data).match(
    /!\[k\[(\w+)[\:\s]*\],v\[(.*)?\]\]!/
  );
  if (!parsedMessage) {
    return null;
  }

  return {
    type: parsedMessage[1],
    values: parsedMessage[2] ? parsedMessage[2].split(",") : [],
  };
};

const socket = new WebSocket("ws://127.0.0.1:12749", [], wsOptions);
socket.addEventListener("message", (message) => {
  const parsedMessage = parseMessage(message);
  if (parsedMessage.type === "alive") {
    socket.send("![k[im alive],v[null]]!");
    return;
  }

  const pos = rl.getCursorPos();
  readline.moveCursor(process.stdout, 0, -1);

  rl.write(
    `\n${chalk.blue(format(new Date(), "HH:mm:ss.SS"))}  ${chalk.yellow(
      chalk.bold(parsedMessage.type.padEnd(20))
    )}  ${chalk.white(parsedMessage.values.join(", "))}\n`
  );

  readline.cursorTo(process.stdout, pos.x, pos.y);
});

const getInput = () => {
  rl.question("> ").then((input) => {
    socket.send(input);
    getInput();
  });
};

getInput();

# HiveMind Client

### Test Lab

The `docker-compose` file will simulate four cabinets running games at random and a client that connects to your local instance.

    docker-compose up -d

On some systems you may need to edit `config.json` to replace the underscores with dashes in each cabinet's `url` field. Try this if the client logs show `getaddrinfo ENOTFOUND hivemind-client_cabinet_1`.
